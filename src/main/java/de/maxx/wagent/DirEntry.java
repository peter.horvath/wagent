package de.maxx.wagent;

import java.nio.file.WatchKey;

public class DirEntry extends Entry {
    private WatchKey key;

    DirEntry(String strPath, WatchKey key) {
        super(strPath);
        this.key = key;
    }

    public WatchKey getKey() {
        return key;
    }
}