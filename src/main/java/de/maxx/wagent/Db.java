package de.maxx.wagent;

import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class Db {
    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    Db(Properties properties) {
        Map<String, String> pMap = new HashMap<>();
        Enumeration<?> e = properties.propertyNames();
        while (e.hasMoreElements()) {
            String s = (String) e.nextElement();
            if (!s.startsWith("hibernate.")) {
                continue;
            }
            pMap.put(s, properties.getProperty(s));
        }

        entityManagerFactory = Persistence.createEntityManagerFactory("wagent", pMap);
        
        entityManager = entityManagerFactory.createEntityManager();
    };

    void storeEvent(String agentId, String siteUrl, String kind, String rawPath, String extension, String fileType) {
        EventEntity event = new EventEntity();
        event.setAgentId(agentId);
        event.setSiteUrl(siteUrl);
        event.setKind(kind);
        event.setRawPath(rawPath);
        event.setExtension(extension);
        event.setFileType(fileType);
        event.setTimestamp(new Date());
        
        SessionFactory sessionFactory = (SessionFactory)entityManagerFactory;
        Session session = sessionFactory.openSession();
        
        session.save(event);
        session.close();
    }
}