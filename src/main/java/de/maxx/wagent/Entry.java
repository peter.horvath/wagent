package de.maxx.wagent;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.TreeSet;

public class Entry {
    public static Comparator<Entry> direntComparator = new Comparator<Entry>() {
        @Override
        public int compare(Entry w1, Entry w2) {
            return w1.getName().compareTo(w2.getName());
        }
    };

    private DirEntry parent;
    private String name;
    private TreeSet<Entry> children = new TreeSet<Entry>();

    Entry(String name, DirEntry parent) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public DirEntry getParent() {
        return parent;
    }

    public String getFullPath() {
        LinkedList<String> retList = new LinkedList<String>();
        Entry dirent = this;
        do {
            retList.addFirst(dirent.getName());
            dirent = dirent.getParent();
        } while (dirent != null);
        return String.join(java.io.File.separator, retList);
    }

    public static Comparator<Entry> comparator() {
        return direntComparator;
    }
}