package de.maxx.wagent;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;

public class App {
    private Fs fs = null;
    private Db db = null;
    private Properties properties = new Properties();
    private String agentId = "default";
    private List<Site> sites = new LinkedList<Site>();
    private List<Pattern> excludedFiles = new LinkedList<Pattern>();
    private List<String> excludedFolders = new LinkedList<String>();

    private static void usage() {
        System.out.println("At most 1 arg can be given to the config.properties path");
        System.exit(1);
    }

    private String getDefaultConfigPath() {
        String jarPath = App.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        String jarDir = new File(jarPath).getParent();
        String configPath = Paths.get(jarDir, "config.properties").toString();
        return configPath;
    }

    private void loadProperties(String configPath) {
        System.out.println("Loading properties from " + configPath);
        try {
            InputStream propStream = new FileInputStream(configPath);
            properties.load(propStream);
            agentId = properties.getProperty("agent_id");
            if (agentId == null) {
                throw new RuntimeException("please put the agent id into the agent_id property");
            }

            Enumeration<?> e = properties.propertyNames();
            while (e.hasMoreElements()) {
                String siteUrlKey = (String) e.nextElement();
                if (Pattern.matches("site_.*_url", siteUrlKey)) {
                    String siteUrl = properties.getProperty(siteUrlKey);
                    System.out.println("siteUrl: " + siteUrl);
                    String basePathName = siteUrlKey.substring(0, siteUrlKey.lastIndexOf("_")) + "_path";
                    String basePath = properties.getProperty(basePathName);
                    if (basePath == null) {
                        throw new RuntimeException("please define " + basePathName);
                    }
                    sites.add(new Site(siteUrl, basePath));
                } else {
                    continue;
                }
            }

            String excludeFilesStr = properties.getProperty("excluded_files");
            if (excludeFilesStr != null) {
                for (String s : excludeFilesStr.split(";")) {
                    excludedFiles.add(Pattern.compile(Utils.wildcardToRegex(s)));
                }
            }

            String excludeFoldersStr = properties.getProperty("excluded_folders");
            if (excludeFoldersStr != null) {
                excludedFolders = Arrays.asList(excludeFoldersStr.split(";"));
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public List<String> getSiteRoots() {
        List<String> siteRoots = new LinkedList<String>();
        for (Site site : sites) {
            siteRoots.add(site.getRootPath());
        }
        return siteRoots;
    }

    private String getUrlFromPath(String path) {
        for (Site site : sites) {
            if (site.ownsPath(path)) {
                return site.getUrl();
            }
        }
        throw new RuntimeException("Path " + path + " does not belong to any known sites");
    }

    public boolean isExcludedFile(String path) {
        String baseName = FilenameUtils.getBaseName(path);
        for (Pattern p : excludedFiles) {
            if (p.matcher(baseName).matches()) {
                return true;
            }
        }
        return false;
    }

    public boolean isInExcludedFolder(String path) {
        for (String folder : excludedFolders) {
            if (path.startsWith(folder + "/") || path.startsWith(folder + "\\"))
                return true;
        }
        return false;
    }

    public void onEvent(String kind, String path) {
        System.out.println("event " + kind + " on " + path);

        if (isExcludedFile(path) || isInExcludedFolder(path)) {
            System.out.println("excluded file, ignored");
            return;
        }

        String fileType = "F";
        String rawPath;
        String extension;
        if (new File(path).isDirectory()) {
            fileType = "D";
            rawPath = path;
            extension = "";
        } else {
            int lastPtIdx = path.lastIndexOf(".");
            if (lastPtIdx > 0) {
                rawPath = path.substring(0, lastPtIdx);
                extension = path.substring(lastPtIdx + 1);
            } else {
                rawPath = path;
                extension = "";
            }
        }
        String siteUrl = getUrlFromPath(path);

        db.storeEvent(agentId, siteUrl, kind, rawPath, extension, fileType);
    }

    private void run(String[] args) {
        String configPath = null;
        if (args.length > 1) {
            usage();
        } else if (args.length == 1) {
            configPath = args[0];
        } else {
            configPath = getDefaultConfigPath();
        }
        loadProperties(configPath);
        fs = new Fs();
        db = new Db(properties);
        for (Site site : sites) {
            fs.recursiveScan(site.getRootPath());
        }
        fs.processEvents(this);
    }

    public static void main(String[] args) {
        App app = new App();
        app.run(args);
    }
}