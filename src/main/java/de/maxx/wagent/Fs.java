package de.maxx.wagent;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Fs {
    private static Comparator<WatchKey> watchKeyComparator = new Comparator<WatchKey>() {
        @Override
        public int compare(WatchKey k1, WatchKey k2) {
            return k1.toString().compareTo(k2.toString());
        }
    };

    private WatchService watcher = null;
    private Map<String, Entry> watchByAbsPath = new TreeMap<String, Entry>();
    private Map<WatchKey, Entry> watchByKey = new TreeMap<WatchKey, Entry>(watchKeyComparator);

    Fs() {
        try {
            watcher = FileSystems.getDefault().newWatchService();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void makeWatchSingle(String strPath) {
        try {
            Path path = Paths.get(strPath);
            System.out.println("setting up watch for " + strPath);
            WatchKey key = path.register(watcher, StandardWatchEventKinds.ENTRY_CREATE,
                    StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);
            Entry watch = new Entry(strPath, key);
            watchByAbsPath.put(strPath, watch);
            watchByKey.put(key, watch);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void recursiveScan(String strPath) {
        
    }
    
    public void makeWatch(String strPath) {
        File f = new File(strPath);
        if (f.isDirectory()) {
            try {
                Path path = Paths.get(strPath);
                Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult preVisitDirectory(Path dirPath, BasicFileAttributes attrs)
                            throws IOException {
                        String dir = dirPath.toString();
                        makeWatchSingle(dir);
                        return FileVisitResult.CONTINUE;
                    }
                });
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        } else {
            makeWatchSingle(strPath);
        }
    }

    private void removeWatch(String path) {
        Entry watch = watchByAbsPath.get(path);
        if (watch == null) {
            throw new RuntimeException("removeWatch invalid: " + path);
        }
        watch.getKey().cancel();
        watchByAbsPath.remove(path);
        watchByKey.remove(watch.getKey());
    }

    private void onEvent(App app, WatchKey key, WatchEvent<?> event) {
        String kind = event.kind().name();
        String relPath = event.context().toString();
        Entry watch = watchByKey.get(key);
        if (watch == null) {
            throw new RuntimeException("watch event on not watched resource " + relPath);
        }
        String absPath = Paths.get(watch.getPath(), relPath).toString();
        if ("ENTRY_CREATE".equals(kind)) {
            File f = new File(absPath);
            if (f.isDirectory()) {
                System.out.println("Directory created: " + absPath);
                if (app.isInExcludedFolder(absPath)) {
                    System.out.println("...in excluded folder, ignore");
                } else {
                    System.out.println("...setting up new watch");
                    makeWatch(absPath);
                }
            }
        } else if ("ENTRY_DELETE".equals(kind)) {
            if (watchByAbsPath.containsKey(absPath)) {
                System.out.println("Watched directory deleted: " + absPath);
                removeWatch(absPath);
            }
        }
        app.onEvent(kind, absPath);
    }

    public void processEvents(App app) {
        try {
            while (true) {
                WatchKey watchKey = null;
                watchKey = watcher.poll(10, TimeUnit.MINUTES);
                if (watchKey != null) {
                    Stream<WatchEvent<?>> stream = watchKey.pollEvents().stream();
                    List<WatchEvent<?>> events = stream.collect(Collectors.toList());
                    System.out.println("Processing " + events.size() + " events");
                    for (WatchEvent<?> event : events) {
                        onEvent(app, watchKey, event);
                    }
                    watchKey.reset();
                }
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}