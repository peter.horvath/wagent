package de.maxx.wagent;

import java.util.regex.Pattern;

public class Utils {
    public static String stripTailSlashes(String str) {
        while (str.endsWith("/") || str.endsWith("\\"))
            str = str.substring(0, str.length() - 1);
        return str;
    }

    public static String wildcardToRegex(String wildcard) {
        final StringBuilder regex = new StringBuilder(wildcard.length() * 2);
        for (final char c : wildcard.toCharArray()) {
            switch (c) {
            case '?':
                regex.append(".");
                break;
            case '*':
                regex.append(".*");
                break;
            default:
                regex.append(Pattern.quote(String.valueOf(c)));
                break;
            }
        }
        return regex.toString();
    }
}