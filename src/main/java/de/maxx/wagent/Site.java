package de.maxx.wagent;

public class Site {
    private String url;
    private String rootPath;
    
    Site(String url, String rootPath) {
        this.url = url;
        this.rootPath = Utils.stripTailSlashes(rootPath);
    }
    
    String getUrl() {
        return url;
    }
    
    String getRootPath() {
        return rootPath;
    }
    
    boolean ownsPath(String path) {
        return path.startsWith(rootPath + "/") || path.startsWith(rootPath + "\\");
    }
}