Repo for filesystem monitoring agent, Milestone#1

== Compilation ==

The development happened with OpenJDK-11 with a recent maven. Probably it would work in any different Java development environment, but it was not tested.

The command of the compilaton is:

mvn clean package

It creates target/wagent-0.8-jar-with-dependencies.jar (its name does not matter, this is the default what maven compiles). This jar file can be called by the


java -jar target/wagent-0.8-jar-with-dependencies.jar


command. In its current state, it is a command line-only java app yet. Its configuration is always beside the jar file, and is called as "config.properties" . The compilation script creates one by default, which looks so:

```
agent_id=test
excluded_files=*.php,*.log
excluded_folders=/tmp/test/cache
site_1_url=https://test.site/
site_1_path=/home/maxx/wagent/wagent
site_2_url=https://another.test.site/
site_2_path=/tmp/test/
hibernate.dialect=org.hibernate.dialect.PostgreSQLDialect
hibernate.connection.driver_class=org.postgresql.Driver
hibernate.connection.url=jdbc:postgresql://localhost/wagent
hibernate.connection.username=wagent
hibernate.connection.password=wagent
hibernate.show_sql=true
hibernate.hbm2ddl.auto=update
```

The `hibernate.*` keys define the database connection; by default it is a postgresql on the localhost, with "wagent" user/password/database name. Adding mysql/mssql/sqlite/oracle/...anything SQL support would be trivial and will be made in reply-time on ask.


It would be also possible to run it as a windows system service, or as a systemd .service file (on Linuxes).


The `agent_id`, `excluded_files` and `excluded_folders` are doing what was described in the upwork task description.


The `site_*_url`, and `site_*_path` keys define the web URLs, and their local filesystem pathes. After "site_", you can use any string, not only numbers.
